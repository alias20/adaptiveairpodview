#import <substrate.h>
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <dlfcn.h>

@interface NSDistributedNotificationCenter : NSNotificationCenter
@end

static bool hookColor = false;

#define DarkUIColor \
[UIColor colorWithRed:28.0/255.0 green:28.0/255.0 blue:28.0/255.0 alpha:1.0]

@interface ProximityStatusViewController : UIViewController {
	UILabel *titleLabel;
}
- (UIImage *)colorizeImage:(UIImage *)image withColor:(UIColor *)color;
@end

@interface PRXCardContentWrapperView : UIView
@end

@interface PRXTextView : UITextView
@end

@interface PRXCardContentView : UIView
@end

@interface _TtC18SharingViewService17ExitBuddyIconView : UIView
@end

@interface UIImageView ()
@property (nonatomic,copy) UIColor * vk_contentTintColor; 
@end

@interface CALayer ()
-(void)setContentsMultiplyColor:(CGColor *)arg1;
@end

@interface UITraitCollection ()
// +(id)traitCollectionWithUserInterfaceStyle:(long long)arg1;
@end

@interface UIImage ()
-(id)withTintColor:(id)arg1;
- (UIImage *)applyColor:(UIColor *)color toImage:(UIImage *)image;
@end

@interface PRXButton : UIButton
@end

static bool isDarkMode = false;
%group SharingViewServiceHook

void setModeState(void) {
		// [[NSDistributedNotificationCenter defaultCenter] addObserverForName:@"kr.xsf1re.flyjbcenter" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *notification) {
		// // [self performSelector:selector];
		// 	NSLog(@"[AirPodView] setModeState: %@", [notification.userInfo objectForKey:@"modeState"]);
		// }];
		// if([[notification.userInfo objectForKey:@"modeState"] isEqualToString:@"bypassFailedToss"]) {
		// UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"FlyJB X" message:@"토스 계정이 정지될 위험한 상황으로부터 보호되었습니다.\n\n토스 탈옥감지를 우회하는데 실패한 것으로 판단되어 앱을 강제 종료하였습니다." preferredStyle: UIAlertControllerStyleAlert];
		// [alert addAction:[UIAlertAction actionWithTitle:@"확인" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
		//                           [alert dismissViewControllerAnimated:YES completion:nil];
		// 		  }]];
		// [self presentViewController:alert animated:true completion:nil];
}

// "Someones's Airpod" text view
// Called by -[ProximityStatusContentViewController viewDidLoad]
%hook PRXTextView
-(id)initWithStyle:(long long)arg1 {
	id ret = %orig;
	// NSLog(@"[AirPodView] PRXTextView: superview2: %@, trace: %@", self.superview.superview, [NSThread callStackSymbols]);
	if(isDarkMode)
		self.backgroundColor = DarkUIColor;
	
	return ret;
}

-(void)setTextColor:(UIColor *)arg1 {
	if(isDarkMode)
		%orig([UIColor whiteColor]);
}
%end

//WORK 
%hook PRXCardContentWrapperView
-(id)initWithContentView:(id)arg1 {
	id ret = %orig;
	if(isDarkMode) {
		self.backgroundColor = DarkUIColor;
		MSHookIvar<PRXCardContentView *>(self, "_contentView").backgroundColor = DarkUIColor;
	}
	return ret;
}
%end

// %hook UIImage
// +(UIImage *)systemImageNamed:(NSString *)arg1 withConfiguration:(UIImageConfiguration *)arg2 {
// 	if(![arg1 hasPrefix:@"battery"])
// 		NSLog(@"[AirPodView] UIImage systemImageNamed: %@, withConfiguration: %@, trace: %@", arg1, arg2, [NSThread callStackSymbols]);
// 	if([arg1 isEqualToString:@"xmark"])  {
// 		// arg2.traitCollection = [UITraitCollection traitCollectionWithDisplayScale:[UIScreen mainScreen].scale];
// 		// UITraitCollection *tc = arg2.traitCollection;
// 		UIImage *ret = %orig;
// 		// MSHookIvar<UIColor *>(arg2.traitCollection, "_tintColor") = [UIColor whiteColor];
// 		// tc = [UITraitCollection traitCollectionWithUserInterfaceStyle:UIUserInterfaceStyleLight];
// 		// arg2.tintColor = [UIColor whiteColor];
// 		// UIImage * ret = %orig(arg1, arg2);
// 		// UIImage *ret = [%orig(arg1, arg2) imageWithTintColor:[UIColor whiteColor]];
// 		// - (UIImage *)applyColor:(UIColor *)color toImage:(UIImage *)image;
// 		return ret;
// 	}
// 	return %orig;
// }

// se2 15.0.2 [AirPodView] UIImage systemImageNamed: xmark, withConfiguration: pointSize=15, weight=Bold, scale=M, (null), trace: (
// 	0   AdaptiveAirPodView.dylib            0x0000000105165d18 _Z28MultiplyImageByConstantColorP7UIImageP7UIColor + 3300
// 	1   UIKitCore                           0x0000000184e756bc C46A087A-E13A-3AED-A8A1-D4CC4AC9948A + 3462844
// 	2   UIKitCore                           0x0000000184b33744 C46A087A-E13A-3AED-A8A1-D4CC4AC9948A + 46916
// 	3   UIKitCore                           0x0000000184d1deb8 C46A087A-E13A-3AED-A8A1-D4CC4AC9948A + 2055864
// 	4   ProxCardKit                         0x00000001e5bc5348 6194CF91-F2C3-3AFD-A7CF-5E47B5DE1823 + 58184
// 	5   ProxCardKit                         0x00000001e5bd8e54 6194CF91-F2C3-3AFD-A7CF-5E47B5DE1823 + 138836
// 	6   ProxCardKit                         0x00000001e5bd82fc 6194CF91-F2C3-3AFD-A7CF-5E47B5DE1823 + 135932
// 	7   SharingViewService                  0x0000000104d924b0 SharingViewService + 779440
// 	8   SharingViewService                  0x0000000104d931b4 SharingViewService + 782772
// 	9   UIKitCore

//Set color of X button
%hook UIImageView
-(void)setTintColor:(UIColor *)arg1 {
	if(isDarkMode && [self.superview isKindOfClass:NSClassFromString(@"PRXButton")] && arg1 == UIColor.secondaryLabelColor) {
		%orig([UIColor colorWithRed:160.0/255.0 green:160.0/255.0 blue:160.0/255.0 alpha:1.0]);
		return;
	}
	%orig;
}
%end

//CheckPoint
%hook UIImage
+(UIImage*)kitImageNamed:(NSString*)arg1  {
	UIImage * ret = %orig;
	if([arg1 isEqualToString:@"UICloseButtonBackgroundCompact"]) {
		// NSLog(@"[AirPodView] hookColor true");
		hookColor = true;
	}
	return ret;
}

//Set background color of X button
- (UIImage *)imageWithTintColor:(UIColor *)color {
	if(isDarkMode && hookColor) {
		hookColor = false;
		return %orig([UIColor colorWithRed:50.0/255.0 green:50.0/255.0 blue:50.0/255.0 alpha:1.0]);
	}
	return %orig;
}
%end

//WORK, make dark mode 
%hook ProximityStatusViewController

-(void)viewWillAppear:(bool)arg1 {
	%orig;
	if(isDarkMode) {
	// NSLog(@"[AirPodView] ProximityStatusViewController viewWillAppear: %d, superView: %@, s2: %@, s3: %@, s3 subviews: %@", arg1, self.view.superview, self.view.superview.superview, self.view.superview.superview.superview, self.view.superview.superview.superview.subviews);
	// NSLog(@"[AirPodView] self.view.superview.superview.superview.subviews[5].subviews[1].subviews: %@", self.view.superview.superview.superview.subviews[5].subviews[1].subviews);

	self.view.backgroundColor = DarkUIColor;

	MSHookIvar<UILabel *>(self, "mainBatteryLabel").textColor = [UIColor whiteColor];
	MSHookIvar<UILabel *>(self, "leftBatteryLabel").textColor = [UIColor whiteColor];
	MSHookIvar<UILabel *>(self, "rightBatteryLabel").textColor = [UIColor whiteColor];
	MSHookIvar<UILabel *>(self, "bothBatteryLabel").textColor = [UIColor whiteColor];
	MSHookIvar<UILabel *>(self, "caseBatteryLabel").textColor = [UIColor whiteColor];

	UIImageView *imageView = MSHookIvar<UIImageView *>(self, "leftBatteryChargeImageView");
	UIImage *coloredImage = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
	imageView.image = coloredImage;
	imageView.tintColor = [UIColor whiteColor];

	imageView = MSHookIvar<UIImageView *>(self, "rightBatteryChargeImageView");
	coloredImage = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
	imageView.image = coloredImage;
	imageView.tintColor = [UIColor whiteColor];

	imageView = MSHookIvar<UIImageView *>(self, "bothBatteryChargeImageView");
	coloredImage = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
	imageView.image = coloredImage;
	imageView.tintColor = [UIColor whiteColor];

	imageView = MSHookIvar<UIImageView *>(self, "caseBatteryChargeImageView");
	coloredImage = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
	imageView.image = coloredImage;
	imageView.tintColor = [UIColor whiteColor];

	imageView = MSHookIvar<UIImageView *>(self, "mainBatteryChargeImageView");
	coloredImage = [imageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
	imageView.image = coloredImage;
	imageView.tintColor = [UIColor whiteColor];
	}
}

- (void)_updateBatteryLevelLeft:(double)arg1 levelRight:(double)arg2 levelCase:(double)arg3 {
	%orig;
}
%end


%end

%group SBHooks
%hook UIViewController
- (void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection {
    %orig;
	NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
	NSString *modeState = [NSString stringWithFormat:@"%ld", UIScreen.mainScreen.traitCollection.userInterfaceStyle];
	[userInfo setObject:modeState forKey:@"modeState"];
	[[NSDistributedNotificationCenter defaultCenter] postNotificationName:@"alias20.darkmodechecker" object:nil userInfo:userInfo];
}
%end

%hook SBHomeScreenViewController
-(void)loadView {
	%orig;
	[[NSDistributedNotificationCenter defaultCenter] addObserverForName:@"alias20.darkmodechecker" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *notification) {
		if([[notification.userInfo objectForKey:@"modeState"] isEqualToString:@"ask"]) {
			NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
			NSString *modeState = [NSString stringWithFormat:@"%ld", UIScreen.mainScreen.traitCollection.userInterfaceStyle];
			[userInfo setObject:modeState forKey:@"modeState"];
			[[NSDistributedNotificationCenter defaultCenter] postNotificationName:@"alias20.darkmodechecker" object:nil userInfo:userInfo];
		}
	}];
}
%end
%end

%ctor {
	@autoreleasepool {
		NSString *bundleID = [[NSBundle mainBundle] bundleIdentifier];
		if([bundleID isEqualToString:@"com.apple.springboard"])
			%init(SBHooks);
		if([bundleID isEqualToString:@"com.apple.SharingViewService"]) {
			NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
			[userInfo setObject:@"ask" forKey:@"modeState"];
			[[NSDistributedNotificationCenter defaultCenter] postNotificationName:@"alias20.darkmodechecker" object:nil userInfo:userInfo];



			[[NSDistributedNotificationCenter defaultCenter] addObserverForName:@"alias20.darkmodechecker" object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *notification) {
			// [self performSelector:selector];
				// NSLog(@"[AirPodView] setModeState: %@", [notification.userInfo objectForKey:@"modeState"]);
				if([[notification.userInfo objectForKey:@"modeState"] isEqualToString:@"1"]) {
					isDarkMode = false;
				}
				if([[notification.userInfo objectForKey:@"modeState"] isEqualToString:@"2"]) {
					isDarkMode = true;
				}
			}];
			%init(SharingViewServiceHook);
		}
		// NSLog(@"[AirPodView] %ld", UIScreen.mainScreen.traitCollection.userInterfaceStyle);
	}
}